# -*- coding: utf-8 -*-
{
    'name': "Odoo ETL Demo",

    'application': True,

    'summary': """
        A demo ETL project for showcasing the Odoo ETL Shell Module""",

    'description': """
        A demo ETL project for showcasing the Odoo ETL Shell Module
        A rudimentary framework that serves as a basis for creating complex Odoo ETL modules


        https://github.com/idazco/odoo_etl_shell
    """,

    'author': "community",
    'website': "https://github.com/idazco/odoo_etl_demo",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Technical Settings',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'odoo_etl_shell'],

    # always loaded
    'data': [
        'views/res_config.xml',
        'data/cron_data.xml',
        'data/steps_demo.xml',
    ],
    'qweb': [],
    # only loaded in demonstration mode
    'demo': [
    ],
}